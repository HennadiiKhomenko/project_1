<!doctype HTML>
<?php
$timestamp = date('Y-m-d H:i:s', time());
if (isset($_POST['name']) && isset($_POST['description']) && isset($_POST['created'])) {
    $pdo = new PDO("pgsql:host=127.0.0.1;dbname=crud", 'postgres', 'postgres');
    $stmt = $pdo->prepare('INSERT INTO article (name, description, created_at) VALUES (:name,:description,:created)');
    $stmt->bindValue(':name', $_POST['name']);
    $stmt->bindValue(':description', $_POST['description']);
    $stmt->bindValue(':created', $_POST['created']);
    $stmt->execute();
}
?>
<html lang="ru_RU.UTF-8">
<head>
	<meta charset="UTF-8">
	<title>Create page</title>
</head>
<body>
	<form action="" method="post">
	<p>Название статьи  <input type="text" name="name"/></p>
    <p>Описание статьи  <input type="text" name="description"/></p>
    <p>Время внесения информации  <input type="text" name="created" value="<?php echo $timestamp ?>"/></p>
    <p><input type="submit" value="Записать" </p>
    </form>
        <br>
        <form method="LINK" action="/index.html">
            <input type="submit" value="Назад">
        </form>
</body>
</html>

